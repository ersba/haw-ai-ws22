# Umfang

90 Minuten  
Keine Hilfsmittel erlaubt  
Aufgabenblatt enthällt Papier, man muss nur einen Stift mitbringen

# Mögliche Klausuraufgaben

## Welche Fähigkeiten sollte ein Architekt haben?

3 von 7 aufzählen reicht

Beschreibung in [01_BAI5-AI_Einführung](https://gitlab.com/Regenhardt/haw-ai-ws22/-/blob/main/Skript/01_BAI5-AI_Einf%C3%BChrung.pdf) Seite 36.

## Kontext-/Verteilungs-/Bausteinsicht erstellen

Level aus der Vorlesung erwartet (waren ca. 8 Komponenten, 4-6 wären wohl auch ok je nach Aufgabe).  
Nicht nur Bausteine sondern auch deren Abhängigkeiten zueinander.

## Event Storming

Wie funktioniert das und was macht man mit dem Ergebnis?  
Kontexte aus gegebenem Event Storming Board ableiten.

Beschreibung in [02_BAI5-AI_Methodik1_Anforderungen_und_Randbedigungen](https://gitlab.com/Regenhardt/haw-ai-ws22/-/blob/main/Skript/02_BAI5-AI_Methodik1_Anforderungen_und_Randbedigungen.pdf).

## System (z.B. EKS aus der Vorlesung) wird vorgestellt (ca. eine Seite), hierzu die Sichten erstellen

## Patterns  

Pros/Cons eines bestimmten Patterns (z.B. Microservices)

Was spricht in einem Beispiel für oder gegen ein bestimmtes Pattern?

# Grobe Idee haben

## Was ist ein Informationssystem?

## Was ist Architektur?

Jedes System braucht eine Architektur um Nicht-Funktionale Anforderungen (NFRs) zu klären.

Beschreibungen in [01_BAI5-AI_Einführung](https://gitlab.com/Regenhardt/haw-ai-ws22/-/blob/main/Skript/01_BAI5-AI_Einf%C3%BChrung.pdf).

## Was ist Arc42?

## Patterns

## Unterschied Agil/Klassisch

## DDD/Event Storming

## Historisches nicht wichtig

## Qualitätsanforderungen und -bäume

## Was sind Randbedingungen?

## Sichten

## Heuristiken

Designmuster + Prinzipien

## Persistenz

Persistenzmöglichkeiten, Vorteile + Nachteile  
CAP Theorem

## Systemintegration

Asynchron, Synchron  
API-Möglichkeiten  
Keine Message-Patterns (nicht wichtig bzw. zu spezifisch)